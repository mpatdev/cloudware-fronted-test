import Vue from 'vue'
import Vuex from 'vuex'
import API from "@/api"
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userProps: ["photo", "title", "firstname", "lastname", "gender", "email", "phone", "cell"],
    users: [],
  },
  mutations: {
    ADD_PROP(state, prop) {
      if(!state.userProps.includes(prop)){
        state.userProps.push(prop);
      }
    },
    DROP_PROP(state, prop) {
      if(state.userProps.includes(prop)){
        state.userProps = state.userProps.filter(item => item != prop);
      }
    },
    SET_USERS(state, users){
      if(!localStorage.getItem('users')){
        localStorage.setItem('users', JSON.stringify(users));
      }
      state.users = JSON.parse(localStorage.getItem('users'));
    }
  },
  actions: {
    getUsers({ commit }){
      API().get("?results=10")
      .then(data => data.data.results)
      .then(results => commit("SET_USERS", results));
    }
  }
})
