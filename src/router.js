import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import User from './views/User.vue'
import NotFound from "./views/404.vue";

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/user/:uuid',
      name: 'user',
      component: User
    },
    { path: '/404', component: NotFound },
    { path: '*', redirect: '/404' }
  ]
})
